from django.contrib import admin
from todos.models import TodoItem, TodoList

class TodoItemAdmin(admin.ModelAdmin):
    pass

class TodoListAdmin(admin.ModelAdmin):
    pass

admin.site.register(TodoList, TodoListAdmin)


# Register your models here.
